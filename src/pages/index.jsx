/* global tw */
import React from 'react';
import styled, { css } from 'react-emotion';
import { Parallax, ParallaxLayer } from 'react-spring';

import SEO from '../components/SEO';

const StickyNav = styled.nav`
  ${tw('sticky pin-t bg-white flex p-6 shadow-md z-10')};
`;
const LeftNav = styled.div``;
const RightNav = styled.div``;

const NavTitle = styled.a`
  ${tw('font-semibold')};
`;

const NavLink = styled.a`
  ${tw('text-grey-lighter px-4')};
  transition-property: color;
  transition-duration: 0.2s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);

  :hover {
    ${tw('text-grey cursor-pointer')};
  }
`;

const Content = styled(ParallaxLayer)`
  ${tw('p-6 md:p-12 lg:p-24 justify-center items-center flex z-50')};
`;

const Divider = styled(ParallaxLayer)`
  ${tw('absolute w-full h-full')};
  background: ${props => props.bg};
`;

const Inner = styled.div`
  ${tw('bg-white rounded-lg shadow px-24 py-16')};
`;

const Hero = styled.div`
  ${tw('w-full xl:w-2/3')};
`;

const BigTitle = styled.h1`
  ${tw('text-5xl lg:text-6xl font-serif mb-6 tracking-wide')};
`;

const Subtitle = styled.p`
  ${tw('text-2xl lg:text-4xl font-sans mt-8 xxl:w-3/4')};
`;

const Header = styled.h2`
  ${tw('text-xl lg:text-2xl font-sans')};
`;

const Button = styled.button`
  ${tw('rounded-full text-base bg-blue font-medium p-4 border-0 text-white px-8')};
  transition-property: box-shadow, transform;
  transition-duration: 0.2s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);

  :hover {
    ${tw('cursor-pointer shadow-md')};
    transform: translate(0, -3px);
  }
`;

class Index extends React.Component {
  scroll = to => this.node.scrollTo(to);

  getRef = node => {
    this.node = node;
  };

  render() {
    return (
      <React.Fragment>
        <SEO />
        <StickyNav>
          <LeftNav>
            <NavTitle onClick={() => this.scroll(0)}>Luke Bayliss</NavTitle>
          </LeftNav>
          <RightNav>
            <NavLink onClick={() => this.scroll(1)}>About</NavLink>
            <NavLink onClick={() => this.scroll(2)}>Resume</NavLink>
            <NavLink onClick={() => this.scroll(3)}>Contact</NavLink>
            <NavLink>Blog</NavLink>
          </RightNav>
        </StickyNav>
        <Parallax pages={4} ref={this.getRef}>
          {/* Home */}
          <Content offset={0} speed={0.1}>
            <Hero>
              <BigTitle>Luke Bayliss</BigTitle>
              <Subtitle>Full Stack .NET Developer</Subtitle>
            </Hero>
          </Content>
          {/* About */}
          <Content offset={1} speed={0.3}>
            <Inner>
              <Header>About</Header>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Numquam, deserunt, mollitia recusandae unde
                sequi molestias nisi iusto blanditiis quae nemo incidunt pariatur nobis voluptas quia perspiciatis,
                molestiae explicabo libero natus. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias animi
                voluptatem laudantium ipsa illum, voluptas iste. Non harum nemo commodi? Aspernatur ipsam et vitae
                asperiores at quo iste temporibus iusto.
              </p>
              <Button>Visit Blog</Button>
            </Inner>
          </Content>
          {/* Resume */}
          <Content offset={2} speed={0.3}>
            <Inner>
              <Header>Resume</Header>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Numquam, deserunt, mollitia recusandae unde
                sequi molestias nisi iusto blanditiis quae nemo incidunt pariatur nobis voluptas quia perspiciatis,
                molestiae explicabo libero natus. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias animi
                voluptatem laudantium ipsa illum, voluptas iste. Non harum nemo commodi? Aspernatur ipsam et vitae
                asperiores at quo iste temporibus iusto.
              </p>
              <Button>Request Resume</Button>
            </Inner>
          </Content>
          {/* Contact */}
          <Content offset={3} speed={0.3}>
            <Inner>
              <Header>Contact</Header>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Numquam, deserunt, mollitia recusandae unde
                sequi molestias nisi iusto blanditiis quae nemo incidunt pariatur nobis voluptas quia perspiciatis,
                molestiae explicabo libero natus. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias animi
                voluptatem laudantium ipsa illum, voluptas iste. Non harum nemo commodi? Aspernatur ipsam et vitae
                asperiores at quo iste temporibus iusto.
              </p>
              <Button>Email Me</Button>
            </Inner>
          </Content>
        </Parallax>
      </React.Fragment>
    );
  }
}

export default Index;
