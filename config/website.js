const tailwind = require('../tailwind');

module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "/portfolio"

  siteTitle: 'Luke Bayliss - Full Stack Developer', // Navigation and Site Title
  siteTitleAlt: 'LPB', // Alternative Site title for SEO
  siteUrl: 'https://www.lukebayliss.com', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteLogo: '/logos/logo-400x400.png', // Used for SEO and manifest
  siteDescription: 'Luke Bayliss, developer extraordinaire',

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: '@lukepbayliss', // Twitter Username
  ogSiteName: 'Luke', // Facebook Site Name
  ogLanguage: 'en_US', // Facebook Language

  // Manifest and Progress color
  themeColor: tailwind.colors.blue,
  backgroundColor: tailwind.colors.pink,
};
